#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever.
 *
 * @return int  Application return code (zero for success).
 */

// toggle value to change LED state
int toggle = 1;

int main()
{

    // Specify the PIN number and sleep delay
    const uint LED_PIN = 25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true)
    {

        blinkLED(LED_PIN, LED_DELAY);
    }

    // Should never get here due to infinite while-loop.
    return 0;
}

void blinkLED(uint _led_pin, uint _led_delay)
{

    // change the toggle value
    toggle = !toggle;

    // Toggle the LED and then sleep for delay period
    gpio_put(_led_pin, toggle);
    sleep_ms(_led_delay);
}
